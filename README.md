# LineaTecnologica

Proyecto Linea Tecnologica.

LTSales  es un sistema que permite gestionar y administrar a sus clientes, ventas (equipos de cómputo y accesorios) y mantenimientos programados por parte del gerente, así mismo que permita administrar la información referente a cada uno de los clientes de Línea Tecnológica.
Los clientes de Línea Tecnológica se encuentran agrupados por compañías, instituciones, PYMES o público en general. Cada cliente tendrá un historial de actividades entre ellas, él ultimó mantenimiento o servicio, precio, ventas de accesorios (si se hizo o detalles), gastos por transporte o comida, abonos o un solo pago y próxima visita.